var webpack = require('webpack');

var ExtractTextPlugin = require('extract-text-webpack-plugin');

var stylusLoader = ExtractTextPlugin.extract("style-loader", "css-loader!autoprefixer-loader!stylus-loader?import=" + process.cwd() + "/css/variables");

var plugins = [
    new ExtractTextPlugin("[name].css"),
    new webpack.optimize.CommonsChunkPlugin("vendor", "vendor.bundle.js")
];

if (process.env.NODE_ENV === 'production') {

  plugins.push(new webpack.DefinePlugin({
    'process.env': {

      'NODE_ENV': JSON.stringify('production'),
    },
  }));

  plugins.push(new webpack.optimize.DedupePlugin());
  plugins.push(new webpack.optimize.UglifyJsPlugin());
}

module.exports = {
    entry: {
        main: ["./index.js", "./css/index.styl"],
        vendor: ['react', 'react-intl', 'react-router']
    },
    output: {
        path: "./public",
        filename: "[name].js"
    },
    module: {
        loaders: [{
            test: /\.styl$/,
            loader: stylusLoader
        }, {
            test: /\.js.?$/, // .js & .jsx
            loader: 'jsx-loader?harmony'
        }]
    },
    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".jsx", ".js", ".styl"],
        root: process.cwd() + "/lib"
    },
    plugins: plugins
};
