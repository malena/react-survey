module.exports = {
    Application: require('./application'),
    Rating: require('./rating'),
    Flavours: require('./flavours'),
    Comments: require('./comments') 
};