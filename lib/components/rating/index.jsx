/**
 * Rating 
 *
 * @module Rating 
 *
 * @typeof Component
 * @requires react
 */
var React = require('react');

var internals = {

};

internals.component = {

    /**
     * Render the component
     *
     * @method
     * @return {React.Element}
     */
    render: function () {
        return (
            <div>
                <h2>Do you Approve?</h2>
                <form className="ratingForm" onSubmit={this.handleSubmit}>
                    <input type="radio" ref="approve" value="yes">Yes</input>
                    <input type="radio" ref="approve" value="no">No</input>
                    <input type="radio" ref="approve" value="not sure">Not Sure</input>
                    <input type="submit" value="Submit" />
                </form>
            </div>
        );
    }
};

module.exports = React.createClass(internals.component);
