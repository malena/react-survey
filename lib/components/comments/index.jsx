/**
 * Comments
 *
 * @module Comments 
 *
 * @typeof Component
 * @requires react
 */
var React = require('react');

var internals = {

};

internals.component = {

    /**
     * Render the component
     *
     * @method
     * @return {React.Element}
     */
    render: function () {
        return (
            <div>
                <h2>Comments</h2>
                <form className="commentForm" onSubmit={this.handleSubmit}>
                    <input type="text" placeholder="Your name" ref="author" />
                    <input type="text" placeholder="Say something..." ref="text" />
                    <input type="submit" value="Submit" />
                </form>
            </div>
        );
    }
};

module.exports = React.createClass(internals.component);
