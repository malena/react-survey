/**
 * Flavours
 *
 * @module Flavours 
 *
 * @typeof Component
 * @requires react
 */
var React = require('react');

var internals = {

};

internals.component = {

    /**
     * Render the component
     *
     * @method
     * @return {React.Element}
     */
    render: function () {
        return (
            <div>
                <h2>What did you taste?</h2>
                <form onSubmit={this.handleSubmit}>
                    <input type="checkbox" name="bitter" value="yes">Bitter</input>
                    <input type="checkbox" name="soure" value="no">Sour</input>
                    <input type="checkbox" name="sweet" value="not sure">Sweet</input>
                    <input type="checkbox" name="tart" value="not sure">Tart</input>
                    <input type="checkbox" name="strong" value="not sure">Strong</input>
                    <input type="submit" value="Submit" />
                </form>
            </div>
        );
    }
};

module.exports = React.createClass(internals.component);
