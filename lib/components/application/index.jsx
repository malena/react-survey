/**
 *  Application Wrapper
 *
 * @module Application
 *
 * @typeof Component
 * @requires react
 * @requires react-router
 * @requires locale-store
 */
var React = require('react');
var Router = require('react-router');

var {RouteHandler, Link} = Router;

var internals = {

};

internals.component = {

    /**
     * Render the component
     *
     * @method
     * @return {React.Element}
     */
    render: function () {
        return (
            <div>
                <h1>Beer Kitchen</h1>
                <ul>
                    <li><Link to="rating">Rating</Link></li>
                    <li><Link to="flavours">Flavours</Link></li>
                    <li><Link to="comments">Comments</Link></li>
                </ul>
                <RouteHandler />
            </div>
        );
    }
};

module.exports = React.createClass(internals.component);
