var React = require('react');
var Router = require('react-router');
var {Route, DefaultRoute} = Router;

var {Application, Rating, Flavours, Comments} = require('./components');

var routes = (
    <Route name="home" path="/" handler={Application} >
        <DefaultRoute handler={Rating} />
        <Route name="rating" path="/rating" handler={Rating} />
        <Route name="flavours" path="/flavours" handler={Flavours} />
        <Route name="comments" path="/comments" handler={Comments} />
    </Route>
);

Router.run(routes, Router.HistoryLocation, function renderContent (Handler) {

    React.render(<Handler />, document.getElementById('content'));
});